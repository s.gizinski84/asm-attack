BITS 64

SECTION .data
	SYS_OPEN: equ 2
	SYS_READ: equ 0
	SYS_CLOSE: equ 3
	O_RDONLY: equ 0
	buffer_size: equ 4096
	set_size: equ 256

SECTION .bss
	read_size: resq 1
	buffer: resb buffer_size
	filename: resq 1
	file_desc: resq 1
	pattern: resd 5

SECTION .text

_read:
	mov rax, SYS_READ
	mov rdi, [file_desc]             ; load file descriptor
	mov rsi, buffer                  ; buffer 
	mov rdx, [read_size]             ; chars to be read
	syscall
	ret

_open:
	mov rax, SYS_OPEN
	mov rdi, [filename]
	mov rdx, O_RDONLY
	xor rsi, rsi
	syscall
	ret

_close:
	mov rax, SYS_CLOSE
	mov rdi, [file_desc]            ; load file descriptor
	syscall
	ret

global _start

_start:
	; init pattern
	mov dword[pattern], 6
	mov dword[pattern+4], 8
	mov dword[pattern+8], 0
	mov dword[pattern+12], 2
	mov dword[pattern+16], 0

	pop rax                         ; pop  numer of arguments
	cmp rax, 2
	jne exit_error_b4_opening       ; wrong  numer of arguments - exit without closing the file
	
	                                ; correct number of arguments
	pop rax                         ; this is just the path to the binary
	pop rax                         ; this is the file to open
	mov [filename], rax             ; save it, we'll need it later

	;open a file
	call _open
	cmp rax, -1                     ; check if file had opened successfully
	je exit_error_b4_opening
	mov [file_desc], rax			; saves file descriptor	
	
	mov qword[read_size], 256
	
	call _read
	cmp rax, 0						; check if file not empty								
	je exit_error

	;checking if numbers are ok (32 bit)	
	push rax
	and rax, 3						
	jnz exit
	pop rax

    xor r15, r15					;register for sum
	xor r14, r14					;pattern iterator
	xor r9, r9						;flags on first(pattern) and second(interval) bit

	while_read:
		cmp rax, 0
		je while_read_exit

		xor rbp, rbp 					;iterator
		main_loop:
			mov ebx, dword[buffer + rbp]
			bswap ebx
			;inc rbp					;++i
			add rbp, 4
			_process_number:
				mov r8d, ebx
				
				sub r8d, 68020
				jz exit
				jb _not_in_interval
				;checking if x<2^31 
				mov r8d, ebx
				and r8d, 2147483648
				jnz _not_in_interval
				or r9, 2	;set 2 bit flag(interval)

				_not_in_interval: 
				add r15d, ebx 			;add to sum
				
				mov r10, r9
				and r10, 1
				jnz after_pattern		;do not check pattern if already found


				xor r13, r13
				mov r13d, dword[pattern+r14]
				sub r13d, ebx
				je move_in_pattern
				xor r14, r14
				cmp ebx, dword[pattern+r14] ; checking again
				jne after_pattern
				add r14, 4
				jmp after_pattern

				move_in_pattern:
					add r14, 4
					cmp r14, 20				;check if pattern ended
					jne after_pattern		
					;set pattern flag 
					or r9, 1 ;setting first bit(pattern flag)
				after_pattern:
			cmp rbp, rax
			jl main_loop
		;back to read
		cmp rax, [read_size] 					; check if numberof bytes read =  numer of bytes to read
		jne while_read_exit 					; if not, then EOF occurred

		call _read								; read next part of the file to the buffer

		;checking if numbers are ok (32 bit)
		push rax
		and rax, 3
		jnz exit
		pop rax
		
		jmp while_read;			; continue processing
	
	while_read_exit:	
	;checking
	cmp r9, 3	;check if both flags set
	jne exit

	cmp r15d, 68020
	jne exit

exit_success:
	call _close 				;close the file
	mov rax, 60
	mov rdi, 0
	syscall

exit_error:
exit:
exit_debug:
exit_error_mismatch:
	call _close
exit_error_b4_opening:
	mov rax, 60
	mov rdi, 1
	syscall
