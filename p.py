import sys


def unbytize(a):
    r=[]
    l=len(a)//4
    for i in range(int(l)):
        r.append(int.from_bytes(a[i*4:4+i*4], byteorder='big'))
    
    return r


with open(sys.argv[1], 'rb') as f:
    a=f.read()
print(unbytize(a))