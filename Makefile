all: attack.asm
	nasm -f elf64 -o attack.o attack.asm
	ld --fatal-warnings -o attack attack.o

old: old.asm
	nasm -f elf64 -o attack.o attack.asm
	ld --fatal-warnings -o attack attack.o

clean:
	rm attack.o -f
	rm attack -f
